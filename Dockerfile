FROM rust:1.55.0-bullseye AS build
RUN cargo install wrangler wasm-pack worker-build
FROM rust:1.55.0-slim-bullseye
RUN apt update&&apt install -y libcurl4
COPY --from=build /usr/local/cargo/bin/wrangler /usr/local/bin/wrangler
COPY --from=build /usr/local/cargo/bin/wasm-pack /usr/local/bin/wasm-pack
COPY --from=build /usr/local/cargo/bin/worker-build /usr/local/bin/worker-build
RUN rustup target add wasm32-unknown-unknown
ENTRYPOINT [ "/usr/local/bin/wrangler" ]
WORKDIR /build