# Wrangler

OCI Image for the [Wrangler CLI tool](https://crates.io/crates/wrangler) to work with Cloudflare Workers.

# Usage

```sh
docker run -it --rm -v $HOME/.wrangler:/root/.wrangler -v $PWD:/build registry.gitlab.com/fawad/wrangler_docker:latest publish
```